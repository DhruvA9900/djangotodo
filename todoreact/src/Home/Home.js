import React from "react";

import "./Home.css";
import axios from "axios";

class Home extends React.Component {
  state = {
    todoList: [],
    activeItem: {
      id: null,
      title: "",
      completed: false,
    },
    editing: false,
  };

  onSubmit = () => {
    let config = {
      headers: {
        "Content-type": "application/json",
      },
    };
    let url = "http://127.0.0.1:8000/api/task-create/";

    if (this.state.editing == true) {
      url = `http://127.0.0.1:8000/api/task-update/${this.state.activeItem.id}/`;
      console.log(this.state.activeItem);
      this.setState({ editing: false });
    }
    axios
      .post(url, this.state.activeItem, config)
      .then(() => {
        this.fetchtodoList();
        this.setState({
          activeItem: {
            id: null,
            title: "",
            completed: false,
          },
        });
      })
      .catch((err) => {
        console.log(err);
      });
  };
  handleChange = (e) => {
    this.setState({
      activeItem: {
        ...this.state.activeItem,
        title: e.target.value,
      },
    });
  };

  onEdit = (task) => {
    this.setState({
      editing: true,
      activeItem: task,
    });
  };

  onDelete = (id) => {
    axios
      .delete(`http://127.0.0.1:8000/api/task-delete/${id}`)
      .then(() => {
        this.fetchtodoList();
      })
      .catch((err) => {
        console.log(err);
      });
  };

  activeItemNull = () => {
    this.setState({
      activeItem: {
        ...this.state.activeItem,
        id: null,
        title: "",
        completed: false,
      },
    });
  };

  componentDidMount() {
    this.fetchtodoList();
  }

  fetchtodoList = () => {
    axios
      .get("http://127.0.0.1:8000/api/task-list/")
      .then((res) => {
        this.setState({ todoList: res.data });
      })
      .catch((err) => {
        console.log(err);
      });
  };

  strikeUnstrike(task) {
    let config = {
      headers: {
        "Content-type": "application/json",
      },
    };
    let url = `http://127.0.0.1:8000/api/task-update/${task.id}/`;
    task.completed = !task.completed;
    axios
      .post(url, task, config)
      .then(() => {
        this.fetchtodoList();
        this.setState({
          activeItem: {
            id: null,
            title: "",
            completed: false,
          },
        });
      })
      .catch((err) => {
        console.log(err);
      });
  }

  render() {
    const page = this.state.todoList
      .slice(0)
      .reverse()
      .map((item, index) => {
        return (
          <div key={index} className="listMain">
            {!item.completed ? (
              <div className="title" onClick={() => this.strikeUnstrike(item)}>
                <span>{item.title}</span>
              </div>
            ) : (
              <div className="title" onClick={() => this.strikeUnstrike(item)}>
                <strike>{item.title}</strike>
              </div>
            )}
            <p style={{ paddingRight: 0 }}>completed:</p>
            <p className="completed">{item.completed.toString(0)}</p>
            <button
              className="listButton"
              onClick={() => {
                this.onEdit(item);
              }}
            >
              edit
            </button>
            <button
              className="listButton"
              onClick={() => {
                this.onDelete(item.id);
              }}
            >
              delete
            </button>
          </div>
        );
      });
    return (
      <>
        <br />
        <div className="formElement">
          <div className="mainForm">
            <br />
            <div>
              <input
                onChange={this.handleChange}
                id="title"
                name="title"
                type="text"
                value={this.state.activeItem.title}
                placeholder="Add Title"
              />
            </div>
            <div>
              <button onClick={this.onSubmit}>Submit</button>
            </div>
            <br />
          </div>
        </div>
        <div>{page}</div>
      </>
    );
  }
}

export default Home;
